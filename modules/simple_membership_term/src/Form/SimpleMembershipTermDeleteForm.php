<?php

namespace Drupal\simple_membership_term\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Simple membership term entity entities.
 *
 * @ingroup simple_membership
 */
class SimpleMembershipTermDeleteForm extends ContentEntityDeleteForm {

}
