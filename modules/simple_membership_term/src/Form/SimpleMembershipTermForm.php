<?php

namespace Drupal\simple_membership_term\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Simple membership term edit forms.
 *
 * @ingroup simple_membership
 */
class SimpleMembershipTermForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    /* @var $entity \Drupal\simple_membership_term\Entity\SimpleMembershipTerm */
    $entity = $this->entity;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Simple membership term.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Simple membership term.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.simple_membership_term.canonical', ['simple_membership_term' => $entity->id()]);
  }

}
