<?php

namespace Drupal\simple_membership_term\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class SimpleMembershipTermTypeForm.
 *
 * @package Drupal\simple_membership_term\Form
 */
class SimpleMembershipTermTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_membership\Entity\SimpleMembershipTypeInterface[] $simple_membership_types */
    $simple_membership_types = \Drupal::entityTypeManager()->getStorage('simple_membership_type')->loadMultiple();

    $simple_membership_type_options = [];
    foreach ($simple_membership_types as $id => $simple_membership_type) {
      $simple_membership_type_options[$id] = $simple_membership_type->label();
    }

    /** @var \Drupal\simple_membership_term\Entity\SimpleMembershipTermTypeInterface $simple_membership_term_type */
    $simple_membership_term_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $simple_membership_term_type->label(),
      '#description' => $this->t("Label for the Simple membership term type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $simple_membership_term_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_membership_term\Entity\SimpleMembershipTermType::load',
      ],
      '#disabled' => !$simple_membership_term_type->isNew(),
    ];

    $form['simple_membership_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Simple membership Type'),
      '#options' => $simple_membership_type_options,
      '#default_value' => $simple_membership_term_type->getSimpleMembershipType(),
      '#description' => $this->t('Used by all Simple membership terms of this type.'),
      '#required' => TRUE,
    ];

    $form['workflow'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Workflow'),
      '#description' => $this->t('Enter the Simple membership type workflow definition as a yaml snippet.'),
      '#default_value' => Yaml::dump($simple_membership_term_type->getSimpleWorkflow(), 4),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $simple_membership_term_type = $this->entity;
    $simple_membership_term_type->setSimpleWorkflow(Yaml::parse($form_state->getValue('workflow')));
    $status = $simple_membership_term_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Simple membership term type.', [
          '%label' => $simple_membership_term_type->label(),
        ]));
        // Clear the cached plugin definitions to ensure new plugin is found.
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Simple membership term type.', [
          '%label' => $simple_membership_term_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($simple_membership_term_type->toUrl('collection'));
  }

}
