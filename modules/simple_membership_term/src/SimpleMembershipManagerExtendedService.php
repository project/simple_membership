<?php

namespace Drupal\simple_membership_term;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\simple_membership\Exception\SimpleMembershipFeatureNotImplementedException;
use Drupal\simple_membership\Entity\SimpleMembershipInterface;
use Drupal\simple_membership\SimpleMembershipManagerService;
use Drupal\user\Entity\User;

/**
 * Class SimpleMembershipManagerExtendedService.
 *
 * @package Drupal\simple_membership
 */
class SimpleMembershipManagerExtendedService extends SimpleMembershipManagerService {

  /**
   * Simple membership manager service.
   *
   * @var \Drupal\simple_membership\SimpleMembershipManagerService
   */
  protected $simpleMembershipManagerService;

  /**
   * SimpleMembershipManagerServiceOverride constructor.
   *
   * @param \Drupal\simple_membership\SimpleMembershipManagerService $simple_membership_manager_service
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   */
  public function __construct(SimpleMembershipManagerService $simple_membership_manager_service, EntityTypeManager $entity_type_manager) {
    $this->simpleMembershipManagerService = $simple_membership_manager_service;
    parent::__construct($entity_type_manager);
  }

  /**
   * Return a Simple membership term of a specified type if one exists.
   *
   * @param \Drupal\user\Entity\User $user
   * @param string $simple_membership_type
   * @param string $simple_membership_term_type
   * @param bool $create_if_not_found
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\simple_membership_term\Entity\SimpleMembershipTerm|false|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\simple_membership\Exception\SimpleMembershipNotFoundException
   */
  public function getSimpleMembershipTerm(User $user, string $simple_membership_type = '', string $simple_membership_term_type = '', bool $create_if_not_found = FALSE) {

    // Get the Simple membership entity
    $simple_membership = $this->getSimpleMembership($user, $simple_membership_type);

    return $this->getTerm($simple_membership, $simple_membership_term_type, $create_if_not_found);

  }

  /**
   * Returns a Simple membership term.
   *
   * @param \Drupal\simple_membership\Entity\SimpleMembershipInterface $simple_membership
   * @param string $type
   * @param string $state
   * @param bool $create
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\simple_membership_term\Entity\SimpleMembershipTerm|false|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getTerm(SimpleMembershipInterface $simple_membership, string $type = '', string $state = 'active', bool $create = FALSE) {

    // Check for existing Simple membership terms of this type
    $query = $this->entityTypeManager->getStorage('simple_membership_term')->getQuery();
    $query->condition('simple_membership', $simple_membership->id());
    if ($type) {
      $query->condition('type', $type);
    }
    if ($state) {
      $query->condition('state', $state);
    }
    $query->condition('status', 1);

    if ($existing = $query->accessCheck()->execute()) {
      // Load the Simple memberships.
      $simple_membership_terms = $this->entityTypeManager->getStorage('simple_membership_term')->loadMultiple($existing);
      // Return the latest.
      ksort($simple_membership_terms);
      return array_pop($simple_membership_terms);
    }

    if ($create && $type) {
      $account = $simple_membership->getOwner();
      // Create a new Simple membership term for this Simple membership.
      /** @var \Drupal\simple_membership_term\Entity\SimpleMembershipTerm $simple_membership_term */
      $simple_membership_term = $this->entityTypeManager->getStorage('simple_membership_term')->create([
        'simple_membership' => $simple_membership->id(),
        'type' => $type
      ]);
      $simple_membership_term->setOwner($account);
      // Save and return the the new Simple membership term.
      $simple_membership_term->save();
      return $simple_membership_term;
    }

    return FALSE;

  }

  /**
   * @param \Drupal\simple_membership\Entity\SimpleMembershipInterface $simple_membership
   * @param string $simple_membership_term_type
   *
   * @throws \Drupal\simple_membership\Exception\SimpleMembershipFeatureNotImplementedException
   */
  public function extend(SimpleMembershipInterface $simple_membership, string $simple_membership_term_type = '') {

    // TODO: Implement extend() method.
    throw new SimpleMembershipFeatureNotImplementedException('Simple membership "extend" method not implemented');

  }
}
