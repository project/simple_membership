<?php

namespace Drupal\simple_membership_term\Exception;

use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Class SimpleMembershipTermNotFoundException.
 *
 * @package Drupal\simple_membership\Exception
 */
class SimpleMembershipTermNotFoundException extends \Exception {

  /**
   * SimpleMembershipTermNotFoundException constructor.
   *
   * @param \Drupal\simple_membership\Entity\SimpleMembership|null $simple_membership
   *   The Simple membership entity associated with the Simple membership term.
   * @param string $simple_membership_term_type
   *   The Simple membership term type (bundle machine id).
   */
  public function __construct(SimpleMembershipInterface $simple_membership = NULL, $simple_membership_term_type = '') {
    if (empty($simple_membership)) {
      $message = 'No Simple membership term found, no Simple membership specified.';
    }
    else {
      $message = sprintf(
        'No %s Simple membership term found for "%s".',
        $simple_membership_term_type,
        $simple_membership->label()
      );
    }
    parent::__construct($message);
  }

}
