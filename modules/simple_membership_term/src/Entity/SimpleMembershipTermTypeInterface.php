<?php

namespace Drupal\simple_membership_term\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Simple membership term type entities.
 */
interface SimpleMembershipTermTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Return the workflow definition as an array of states and transitions.
   *
   * @return array
   */
  public function getSimpleWorkflow();

  /**
   * Save the workflow definition as an array of states and transitions.
   *
   * @param array $workflow
   *
   * @return $this
   */
  public function setSimpleWorkflow(array $workflow);

  /**
   * Retrieve the label associated with the passed simple workflow state.
   *
   * @param string $workflow_state
   *
   * @return string
   */
  public function getSimpleWorklowStateLabel(string $workflow_state);

  /**
   * String.
   *
   * @return string
   *   String.
   */
  public function getSimpleMembershipType();

  /**
   * DSimple membership type.
   *
   * @param string $simple_membership_type
   *   Simple membership type.
   *
   * @return string
   *   String.
   */
  public function setSimpleMembershipType(string $simple_membership_type);

}
