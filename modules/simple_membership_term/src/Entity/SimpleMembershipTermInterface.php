<?php

namespace Drupal\simple_membership_term\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Simple membership term entities.
 *
 * @ingroup simple_membership
 */
interface SimpleMembershipTermInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Simple membership term type.
   *
   * @return string
   *   The Simple membership term type.
   */
  public function getType();

  /**
   * Gets the Simple membership term name.
   *
   * @return string
   *   Name of the Simple membership term.
   */
  public function getName();

  /**
   * Sets the Simple membership term name.
   *
   * @param string $name
   *   The Simple membership term name.
   *
   * @return \Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface
   *   The called Simple membership term entity.
   */
  public function setName(string $name);

  /**
   * Gets the Simple membership term creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Simple membership term.
   */
  public function getCreatedTime();

  /**
   * Sets the Simple membership term creation timestamp.
   *
   * @param int $timestamp
   *   The Simple membership term creation timestamp.
   *
   * @return \Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface
   *   The called Simple membership term.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Returns the Simple membership term published status indicator.
   *
   * Unpublished Simple membership terms are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Simple membership term is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Simple membership term.
   *
   * @param bool $published
   *   TRUE for Simple membership term to published, FALSE to unpublished.
   *
   * @return \Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface
   *   The called Simple membership term entity.
   */
  public function setPublished(bool $published);

  /**
   * Returns the Simple membership type that this Simple membership term is associated with.
   *
   * Should be implemented per bundle.
   *
   * @return string
   *   Should be implemented per bundle.
   */
  public function getSimpleMembershipType();

  /**
   * Gets the workflow definition array for the Simple membership term type.
   *
   * @return array
   *   The workflow definitioan array.
   */
  public function getSimpleWorkflow();

  /**
   * Get the current formflow state for this Simple membership term.
   *
   * @return string
   *   The current workflow state.
   */
  public function getSimpleWorkflowState();

  /**
   * Set the current workflow state for this Simple membership term.
   *
   * @param string $workflow_state
   *   The new workflow state to set.
   *
   * @return $this
   *
   */
  public function setSimpleWorkflowState(string $workflow_state);

}
