<?php

namespace Drupal\simple_membership_term\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Simple membership term type entity.
 *
 * @ConfigEntityType(
 *   id = "simple_membership_term_type",
 *   label = @Translation("Simple membership term type"),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_membership_term\SimpleMembershipTermTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_membership_term\Form\SimpleMembershipTermTypeForm",
 *       "edit" = "Drupal\simple_membership_term\Form\SimpleMembershipTermTypeForm",
 *       "delete" = "Drupal\simple_membership_term\Form\SimpleMembershipTermTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\simple_membership_term\SimpleMembershipTermTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "simple_membership_term_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "simple_membership_term",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "simple_membership_type",
 *     "workflow",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/simple_membership_term_type/{simple_membership_term_type}",
 *     "add-form" = "/admin/structure/simple_membership_term_type/add",
 *     "edit-form" = "/admin/structure/simple_membership_term_type/{simple_membership_term_type}/edit",
 *     "delete-form" = "/admin/structure/simple_membership_term_type/{simple_membership_term_type}/delete",
 *     "collection" = "/admin/structure/simple_membership_term_type"
 *   }
 * )
 */
class SimpleMembershipTermType extends ConfigEntityBundleBase implements SimpleMembershipTermTypeInterface {

  /**
   * The Simple membership term type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Simple membership term type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Simple membership type workflow definition as an array of states and transitions.
   *
   * @var array
   */
  protected $workflow;

  /**
   * The Simple membership type for this term type.
   *
   * @var string
   */
  protected $simple_membership_type;

  /**
   * {@inheritdoc}
   */
  public function getSimpleMembershipType() {
    return $this->simple_membership_type;
  }

  /**
   * {@inheritdoc}
   */
  public function setSimpleMembershipType(string $simple_membership_type) {
    $this->simple_membership_type = $simple_membership_type;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getSimpleWorkflow() {
    return $this->workflow;
  }

  /**
   * {@inheritDoc}
   */
  public function setSimpleWorkflow(array $workflow) {
    $this->workflow = $workflow;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorklowStateLabel(string $workflow_state) {
    $label = $workflow_state;
    if ($this->workflow) {
      if (isset($this->workflow['states'][$workflow_state]['label'])) {
        $label = $this->workflow['states'][$workflow_state]['label'];
      }
    }
    return $label;
  }

}
