<?php

namespace Drupal\simple_membership_term\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\simple_membership\EventDispatcherTrait;
use Drupal\user\UserInterface;

/**
 * Defines the Simple m embership term entity.
 *
 * @ingroup simple_membership
 *
 * @ContentEntityType(
 *   id = "simple_membership_term",
 *   label = @Translation("Simple membership term"),
 *   bundle_label = @Translation("Simple membership term type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\simple_membership_term\SimpleMembershipTermListBuilder",
 *     "views_data" = "Drupal\simple_membership_term\Entity\SimpleMembershipTermViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_membership_term\Form\SimpleMembershipTermForm",
 *       "add" = "Drupal\simple_membership_term\Form\SimpleMembershipTermForm",
 *       "edit" = "Drupal\simple_membership_term\Form\SimpleMembershipTermForm",
 *       "delete" = "Drupal\simple_membership_term\Form\SimpleMembershipTermDeleteForm",
 *     },
 *     "access" = "Drupal\simple_membership_term\SimpleMembershipTermAccessControlHandler",
 *   },
 *   base_table = "simple_membership_term",
 *   admin_permission = "administer simple_membership_term entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   bundle_entity_type = "simple_membership_term_type",
 *   field_ui_base_route = "entity.simple_membership_term_type.edit_form"
 * )
 */
class SimpleMembershipTerm extends ContentEntityBase implements SimpleMembershipTermInterface {

  use EntityChangedTrait;
  use EventDispatcherTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName(string $name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished(bool $published) {
    $this->set('status', $published);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Simple membership term entity entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Simple membership term entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Simple membership term is published.'))
      ->setDefaultValue(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['workflow_state'] = BaseFieldDefinition::create('simple_workflow_state')
      ->setLabel(t('Workflow state'))
      ->setDescription(t('The Simple membership term\'s simple workflow state.'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'simple_workflow_state',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'simple_workflow_state',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRevisionable(FALSE);

    $fields['simple_membership'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Simple membership'))
      ->setDescription(t('The Simple membership associated with this term.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'simple_membership')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleMembershipType() {
    return 'simple_membership';
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorkflow() {
    return SimpleMembershipTermType::load($this->bundle())->getSimpleWorkflow();
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorkflowState() {
    $value = $this->workflow_state->first() ? $this->workflow_state->first()->getValue() : [];
    return reset($value);
  }

  /**
   * {@inheritdoc}
   */
  public function setSimpleWorkflowState($workflow_state) {
    $this->workflow_state = $workflow_state;
    return $this;
  }

}
