<?php

namespace Drupal\simple_membership_term\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Simple membership term entities.
 */
class SimpleMembershipTermViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // The base data for the simple_membership_term is already defined by parent.
    $data['simple_membership_term']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Simple membership Term'),
      'help' => $this->t('The Simple membership Term ID.'),
    ];

    // Define the relationship from a simple_membership_term entity to a simple_membership.
    $data['simple_membership_term']['simple_membership']['relationship'] = [
      'base' => 'simple_membership',
      'base field' => 'id',
      'label' => $this->t('Simple membership'),
      'title' => $this->t('Simple membership'),
      'id' => 'standard',
    ];

    return $data;
  }

}
