<?php

/**
 * @file
 * Contains simple_membership_term.page.inc.
 *
 * Page callback for Simple membership term entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Simple membership term templates.
 *
 * Default template: simple_membership_term.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_simple_membership_term(array &$variables) {
  // Helpful content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
