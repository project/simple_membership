<?php

namespace Drupal\simple_membership_term_ui\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Simple membership term operations bulk form element.
 *
 * @ViewsField("simple_membership_term_bulk_form")
 */
class SimpleMembershipTermBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No content selected.');
  }

}
