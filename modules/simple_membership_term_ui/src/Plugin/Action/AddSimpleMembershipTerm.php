<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Add a Simple membership term to a Simple membership.
 *
 * @Action(
 *   id = "simple_membership_term_action_add",
 *   label = @Translation("Add Simple membership term to selected Simple memberships"),
 *   type = "simple_membership"
 * )
 */
class AddSimpleMembershipTerm extends SimpleMembershipTermChange {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipInterface $simple_membership = NULL) {
    // Add Simple membership term if Simple membership is valid and does not already have a current Simple membership term of this type.
    if ($simple_membership !== FALSE) {
      // Get the user associated with this Simple membership.
      $account = $simple_membership->getOwner();
      // Get the configured Simple membership term type id.
      $simple_membership_term_type = $this->configuration['simple_membership_term_type'];
      // Check for existing Simple membership terms of this type
      $query = $this->entityTypeManager->getStorage('simple_membership_term')->getQuery();
      $existing = $query
        ->condition('type', $simple_membership_term_type)
        ->condition('simple_membership', $simple_membership->id())
        ->condition('status', 1)
        ->accessCheck()
        ->execute();
      if (!$existing) {
        // Create a new Simple membership for this account.
        /** @var \Drupal\simple_membership_term\Entity\SimpleMembershipTerm $simple_membership_term */
        $simple_membership_term = $this->entityTypeManager->getStorage('simple_membership_term')->create([
          'simple_membership' => $simple_membership->id(),
          'type' => $simple_membership_term_type,
          ]);
        // Set the simple workflow state to first defined value.
        $workflow = $simple_membership_term->getSimpleWorkflow();
        $workflow_states = array_keys($workflow['states']);
        $simple_membership_term->setSimpleWorkflowState(reset($workflow_states));
        // Set the owner to the same user as parent Simple membership.
        $simple_membership_term->setOwner($account);
        // Save the new Simple membership term.
        $simple_membership_term->save();
      }
      else {
        // Replace Simple membership term type id with Simple membership term type entity.
        /** @var \Drupal\simple_membership_term\Entity\SimpleMembershipTermType $simple_membership_term_type */
        $simple_membership_term_type = $this->entityTypeManager->getStorage('simple_membership_term_type')->load($simple_membership_term_type);
        $this->messenger()->addMessage($this->t('Simple membership id=%mid for user %user (%uid) already has a %type Simple membership term',
        ['%mid' => $simple_membership->id(), '%user' => $account->getAccountName(), '%uid' => $account->id(), '%type' => $simple_membership_term_type->label()]));
      }
    }
  }

}
