<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class SimpleMembershipTermTransition extends ActionBase implements ContainerFactoryPluginInterface {

  use DependencyTrait;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {

    /** @var \Drupal\simple_membership\Entity\SimpleMembership $object */
    $access = $object->access('update', $account, TRUE);
    return $return_as_object ? $access : $access->isAllowed();

  }

  /**
   * Do the state transition for the passed Simple membership term.
   *
   * @param string $transition_id
   * @param \Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface $simple_membership_term
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function doTransition(string $transition_id, SimpleMembershipTermInterface $simple_membership_term) {

    // Get the Simple membership term workflow definition array associated with this
    // $simple_membership_term and check that it has a $transition_id transition
    // that can be applied to this simple_membership_term.
    $workflow = $simple_membership_term->getSimpleWorkflow();

    if (isset($workflow['transitions'][$transition_id])) {
      $transition = $workflow['transitions'][$transition_id];
      $current_state = $simple_membership_term->getSimpleWorkflowState();
      if (in_array($current_state, $transition['from'])) {
        $new_state = $transition['to'];
        $simple_membership_term->setSimpleWorkflowState($new_state);
        $simple_membership_term->save();
      }
      else {
        // TODO fire and exception event.
        $this->messenger()->addError($this->t('Specified Simple membership term transition "@transition" not allowed from state "@state"',
          ['@transition' => $transition_id, '@state' => $current_state]));
      }
    }
    else {
      // TODO fire and exception event.
      $this->messenger()->addError($this->t('Specified Simple membership term transition "@transition" is not defined.',
        ['@transition' => $transition_id]));
    }

  }

}
