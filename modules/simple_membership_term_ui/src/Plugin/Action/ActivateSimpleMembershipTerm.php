<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface;

/**
 * Apply "activate" transition to Simple membership term (present in simple_membership_term_default workflow).
 *
 * @Action(
 *   id = "simple_membership_term_transition_activate",
 *   label = @Translation("Simple membership term transition: Activate"),
 *   type = "simple_membership_term"
 * )
 */
class ActivateSimpleMembershipTerm extends SimpleMembershipTermTransition {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipTermInterface $simple_membership_term = NULL) {

    if ($simple_membership_term instanceof SimpleMembershipTermInterface) {
      $this->doTransition('activate', $simple_membership_term);
    }

  }

}
