<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface;

/**
 * Apply "restart" transition to membership term (present in membership_term_default workflow).
 *
 * @Action(
 *   id = "simple_membership_term_transition_restart",
 *   label = @Translation("Simple membership Term Transition: Restart"),
 *   type = "simple_membership_term"
 * )
 */
class RestartSimpleMembershipTerm extends SimpleMembershipTermTransition {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipTermInterface $simple_membership_term = NULL) {

    if ($simple_membership_term instanceof SimpleMembershipTermInterface) {
      $this->doTransition('restart', $simple_membership_term);
    }

  }

}
