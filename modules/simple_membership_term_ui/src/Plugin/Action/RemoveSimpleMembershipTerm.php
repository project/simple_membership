<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Remove a membership term from a membership.
 *
 * @Action(
 *   id = "simple_membership_term_action_remove",
 *   label = @Translation("Remove Simple membership term from selected Simple memberships"),
 *   type = "simple_membership"
 * )
 */
class RemoveSimpleMembershipTerm extends SimpleMembershipTermChange {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipInterface $simple_membership = NULL) {
    // Remove membership if account is valid and already has a membership.
    if ($simple_membership !== FALSE) {
      // Get the user associated with this membership.
      $account = $simple_membership->getOwner();
      // Get the configured membership_term_type id.
      $simple_membership_term_type = $this->configuration['simple_membership_term_type'];
      // Check for existing membership terms of this type.
      $query = \Drupal::entityTypeManager()->getStorage('simple_membership_term')->getQuery();
      $results = $query
        ->condition('type', $simple_membership_term_type)
        ->condition('membership', $simple_membership->id())
        ->condition('status', 1)
        ->accessCheck()
        ->execute();
      // If there are existing membership terms then delete.
      if ($results) {
        $simple_membership_terms = $this->entityTypeManager->getStorage('simple_membership_term')->loadMultiple($results);
        foreach ($simple_membership_terms as $simple_membership_term) {
          $simple_membership_term->delete();
        }
      }
      else {
        // Replace membership_term_type id with membership_term_type entity.
        /** @var \Drupal\simple_membership_term\Entity\SimpleMembershipTermType $simple_membership_term_type */
        $simple_membership_term_type = $this->entityTypeManager->getStorage('simple_membership_term_type')->load($simple_membership_term_type);
        $this->messenger()->addMessage($this->t('Membership id=%mid for user %user (%uid) did not have a %type membership term',
          ['%mid' => $simple_membership->id(), '%user' => $account->getAccountName(), '%uid' => $account->id(), '%type' => $simple_membership_term_type->label()]));
      }
    }
  }

}
