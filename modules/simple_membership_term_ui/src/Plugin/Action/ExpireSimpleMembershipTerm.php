<?php

namespace Drupal\simple_membership_term_ui\Plugin\Action;

use Drupal\simple_membership_term\Entity\SimpleMembershipTermInterface;

/**
 * Apply "expire" transition to Simple membership term (present in simple_membership_term_default workflow).
 *
 * @Action(
 *   id = "simple_membership_term_transition_expire",
 *   label = @Translation("Simple membership term transition: Expire"),
 *   type = "simple_membership_term"
 * )
 */
class ExpireSimpleMembershipTerm extends SimpleMembershipTermTransition {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipTermInterface $simple_membership_term = NULL) {

    if ($simple_membership_term instanceof SimpleMembershipTermInterface) {
      $this->doTransition('expire', $simple_membership_term);
    }

  }

}
