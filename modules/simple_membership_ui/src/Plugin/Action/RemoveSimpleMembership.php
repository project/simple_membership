<?php

namespace Drupal\simple_membership_ui\Plugin\Action;

use Drupal\Core\Session\AccountInterface;

/**
 * Remove a Simple membership from a user.
 *
 * @Action(
 *   id = "simple_membership_user_action_remove",
 *   label = @Translation("Remove Simple membership from selected users"),
 *   type = "user"
 * )
 */
class RemoveSimpleMembership extends SimpleMembershipChange {

  /**
   * {@inheritdoc}
   */
  public function execute(AccountInterface $account = NULL) {
    // Remove membership if account is valid and already has a membership.
    if ($account !== FALSE) {
      // Get the configured membership_type.
      $simple_membership_type = $this->configuration['simple_membership_type'];
      // Check for existing memberships of this type.
      $query = \Drupal::entityTypeManager()->getStorage('simple_membership')->getQuery();
      $results = $query
        ->condition('type', $simple_membership_type)
        ->condition('user_id', $account->id())
        ->accessCheck()
        ->execute();
      // If there are existing memberships then delete.
      if ($results) {
        $simple_memberships = $this->entityTypeManager->getStorage('simple_membership')->loadMultiple($results);
        foreach ($simple_memberships as $simple_membership) {
          $simple_membership->delete();
        }
      }
      else {
        // Replace membership_type id with membership_type entity.
        /** @var \Drupal\simple_membership\Entity\SimpleMembershipType $simple_membership_type */
        $simple_membership_type = $this->entityTypeManager->getStorage('simple_membership_type')->load($simple_membership_type);
        $this->messenger()->addMessage($this->t('User %user (%uid) did not have a %type membership',
          ['%user' => $account->getAccountName(), '%uid' => $account->id(), '%type' => $simple_membership_type->label()]));
      }
    }
  }

}
