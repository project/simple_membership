<?php

namespace Drupal\simple_membership_ui\Plugin\Action;

use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Expire an active Simple membership using the simple_membership_default workflow.
 *
 * @Action(
 *   id = "simple_membership_transition_expire",
 *   label = @Translation("Simple membership transition: Expire"),
 *   type = "simple_membership"
 * )
 */
class ExpireSimpleMembership extends SimpleMembershipTransition {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipInterface $simple_membership = NULL) {

    if ($simple_membership instanceof SimpleMembershipInterface) {
      $this->doTransition('expire', $simple_membership);
    }

  }

}
