<?php

namespace Drupal\simple_membership_ui\Plugin\Action;

use Drupal\Core\Session\AccountInterface;

/**
 * Add a Simple membership to a user.
 *
 * @Action(
 *   id = "simple_membership_user_action_add",
 *   label = @Translation("Add Simple membership to selected users"),
 *   type = "user"
 * )
 */
class AddSimpleMembership extends SimpleMembershipChange {

  /**
   * {@inheritdoc}
   */
  public function execute(AccountInterface $account = NULL) {
    // Add Simple membership if account is valid and does not already have a Simple membership of this type.
    if ($account !== FALSE) {
      // Get the configured simple_membership_type.
      $simple_membership_type = $this->configuration['simple_membership_type'];
      // Check for existing Simple membership
      $query = $this->entityTypeManager->getStorage('simple_membership')->getQuery();
      $existing = $query
        ->condition('type', $simple_membership_type)
        ->condition('user_id', $account->id())
        ->accessCheck()
        ->execute();
      if (!$existing) {
        // Create a new Simple membership for this account.
        /** @var \Drupal\simple_membership\Entity\SimpleMembership $simple_membership */
        $simple_membership = $this->entityTypeManager->getStorage('simple_membership')->create(['type' => $simple_membership_type]);
        // Set the simple workflow state to first defined value.
        $workflow = $simple_membership->getSimpleWorkflow();
        $workflow_states = array_keys($workflow['states']);
        $simple_membership->setSimpleWorkflowState(reset($workflow_states));
        // Set the passed user account as the owner.
        $simple_membership->setOwner($account);
        // Save the new Simple membership.
        $simple_membership->save();
      }
      else {
        // Replace Simple membership_type id with Simple membership type entity.
        /** @var \Drupal\simple_membership\Entity\SimpleMembershipType $simple_membership_type */
        $simple_membership_type = $this->entityTypeManager->getStorage('simple_membership_type')->load($simple_membership_type);
        $this->messenger()->addMessage($this->t('User %user (%uid) already has a %type Simple membership',
        ['%user' => $account->getAccountName(), '%uid' => $account->id(), '%type' => $simple_membership_type->label()]));
      }
    }
  }

}
