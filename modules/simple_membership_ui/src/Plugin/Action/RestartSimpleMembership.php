<?php

namespace Drupal\simple_membership_ui\Plugin\Action;

use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Restart an expired membership using the membership_default workflow.
 *
 * @Action(
 *   id = "simple_membership_transition_restart",
 *   label = @Translation("Simple membership transition: Restart"),
 *   type = "simple_membership"
 * )
 */
class RestartSimpleMembership extends SimpleMembershipTransition {

  /**
   * {@inheritdoc}
   */
  public function execute(SimpleMembershipInterface $simple_membership = NULL) {

    if ($simple_membership instanceof SimpleMembershipInterface) {
      $this->doTransition('restart', $simple_membership);
    }

  }

}
