<?php

namespace Drupal\simple_membership_ui\Plugin\Action;

use Drupal\Core\Action\ConfigurableActionBase;
use Drupal\Core\Entity\DependencyTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class SimpleMembershipChange extends ConfigurableActionBase implements ContainerFactoryPluginInterface {

  use DependencyTrait;

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'simple_membership_type' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $simple_membership_types = $this->entityTypeManager->getStorage('simple_membership_type')->loadMultiple();
    $types = [];
    /** @var \Drupal\simple_membership\Entity\SimpleMembershipType $simple_membership_type */
    foreach ($simple_membership_types as $simple_membership_type) {
      $types[] = $simple_membership_type->id();
    }

    $form['simple_membership_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Simple membership type'),
      '#options' => $types,
      '#default_value' => $this->configuration['simple_membership_type'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    $this->configuration['simple_membership_type'] = $form_state->getValue('simple_membership_type');

  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {

    if (!empty($this->configuration['simple_membership_type'])) {
      $prefix = $this->entityTypeManager->getDefinition('simple_membership_type')->getConfigPrefix() . '.';
      $this->addDependency('config', $prefix . $this->configuration['simple_membership_type']);
    }
    return $this->dependencies;

  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {

    /** @var \Drupal\user\UserInterface $object */
    $access = $object->access('update', $account, TRUE)
      ->andIf($object->roles->access('edit', $account, TRUE));

    return $return_as_object ? $access : $access->isAllowed();

  }

}
