<?php

namespace Drupal\simple_membership_ui\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines a Simple membership operations bulk form element.
 *
 * @ViewsField("simple_membership_bulk_form")
 */
class SimpleMembershipBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No content selected.');
  }

}
