<?php

/**
 * @file
 * Contains simple_membership.page.inc.
 *
 * Page callback for simple_membership entities.
 */

use Drupal\Core\Render\Element;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Prepares variables for simple_membership templates.
 *
 * Default template: simple-membership.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_simple_membership(array &$variables) {
  // Fetch simple_membership Entity Object.
  $simple_membership = $variables['elements']['#simple_membership'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Prepares variables for a custom entity type creation list templates.
 *
 * Default template: simple_membership-content-add-list.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of simple_membership-types.
 *
 * @see block_content_add_page()
 */
function template_preprocess_simple_membership_content_add_list(array &$variables) {
  $variables['types'] = [];
  $query = \Drupal::request()->query->all();
  foreach ($variables['content'] as $type) {
    $variables['types'][$type->id()] = array(
      'link' => Link::fromTextAndUrl($type->label(), Url::fromRoute('entity.simple_membership.add_form', [
        'simple_membership_type' => $type->id()
      ], ['query' => $query])),
      'description' => [
      '#markup' => $type->label(),
      ],
      'title' => $type->label(),
      'localized_options' => [
        'query' => $query,
      ],
    );
  }
}
