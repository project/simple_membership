<?php

namespace Drupal\simple_membership\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Simple membership provider item annotation object.
 *
 * @see \Drupal\simple_membership\Plugin\SimpleMembershipProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class SimpleMembershipProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
