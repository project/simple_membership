<?php

namespace Drupal\simple_membership\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SimpleMembershipAddController.
 *
 * @package Drupal\simple_membership\Controller
 */
class SimpleMembershipAddController extends ControllerBase {

  /**
   * Storage interface for Simple membership entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Storage interface for Simple membership type entities (config).
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $typeStorage;

  /**
   * Construct a SimpleMembershipAddController.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   Storage for Simple membership entities.
   * @param \Drupal\Core\Entity\EntityStorageInterface $type_storage
   *   Storage for Simple membership type (config) entities.
   */
  public function __construct(EntityStorageInterface $storage, EntityStorageInterface $type_storage) {
    $this->storage = $storage;
    $this->typeStorage = $type_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
        $entity_type_manager->getStorage('simple_membership'),
        $entity_type_manager->getStorage('simple_membership_type')
      );
  }

  /**
   * Displays add links for available bundles/types for entity simple_membership .
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the simple_membership
   *   bundles/types that can be added or
   *   if there is only one type/bunlde defined for the site,
   *   the function returns the add page for that bundle/type.
   */
  public function add(Request $request) {
    $types = $this->typeStorage->loadMultiple();
    if ($types && count($types) == 1) {
      $type = reset($types);
      return $this->addForm($type, $request);
    }
    if (count($types) === 0) {
      $link = Link::fromTextAndUrl($this->t('Go to the type creation page'), Url::fromRoute('entity.simple_membership_type.add_form'));
      return [
        '#markup' => $this->t('You have not created any %bundle types yet. @link to add a new type.', [
          '%bundle' => 'Simple membership',
          '@link' => $link->toString(),
        ]),
      ];
    }
    return ['#theme' => 'simple_membership_content_add_list', '#content' => $types];
  }

  /**
   * Presents the creation form for simple_membership entities of given bundle/type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $simple_membership_type
   *   The custom bundle to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by drupal_render().
   */
  public function addForm(EntityInterface $simple_membership_type, Request $request) {
    $entity = $this->storage->create([
        'type' => $simple_membership_type->id()
      ]);
    return $this->entityFormBuilder()->getForm($entity);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\Core\Entity\EntityInterface $simple_membership_type
   *   The custom bundle/type being added.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getAddFormTitle(EntityInterface $simple_membership_type) {
    return $this->t('Create of bundle @label',
        ['@label' => $simple_membership_type->label()]
      );
  }

}
