<?php

namespace Drupal\simple_membership;

use Drupal\Component\EventDispatcher\Event;
use Drupal\simple_membership\Entity\SimpleMembershipInterface;

/**
 * Class SimpleMembershipEvents.
 *
 * Events thrown on particular Simple membership transitions.
 *
 * @package Drupal\simple_membership
 */
class SimpleMembershipEvent extends Event {

  /**
   * Simple membership interface.
   *
   * @var \Drupal\simple_membership\Entity\SimpleMembershipInterface
   */
  protected $simple_membership;

  /**
   * Construct a SimpleMembershipEvent.
   *
   * @param \Drupal\simple_membership\Entity\SimpleMembershipInterface $simple_membership
   */
  public function __construct(SimpleMembershipInterface $simple_membership) {
    $this->simple_membership = $simple_membership;
  }

  /**
   * Simple membership interface.
   *
   * @return \Drupal\simple_membership\Entity\SimpleMembershipInterface
   *   The generated Simple membership, if available.
   */
  public function getSimpleMembership() {
    return $this->simple_membership;
  }

}
