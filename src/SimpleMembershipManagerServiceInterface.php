<?php

namespace Drupal\simple_membership;

use Drupal\user\Entity\User;

/**
 * Interface SimpleMembershipManagerServiceInterface.
 *
 * @package Drupal\simple_membership
 */
interface SimpleMembershipManagerServiceInterface {

  /**
   * Return the Simple membership for a user, optionally of the specified type.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user entity for this user.
   * @param string $type
   *   The Simple membership type (bundle machine id). If blank,
   *   returns the first Simple membership found.
   * @param string $state
   *   The current state of a Simple membership to search for and return.  If
   *   this value is NULL then a Simple membership in any state is returned.
   * @param bool $create
   *   If TRUE, create a new Simple membership of the specified type for this
   *   user if one does not exist.
   *   If FASLE, throw a SimpleMembershipNotFoundException if no Simple membership exists.
   *
   * @return \Drupal\simple_membership\Entity\SimpleMembershipInterface
   *   The Simple membership for this user. If one does not exist,
   *   a blank Simple membership object.
   *
   * @throws \Drupal\simple_membership\Exception\SimpleMembershipNotFoundException
   *   If a Simple membership is not found, and $create_if_not_found is false,
   *   throws this exception.
   */
  public function getSimpleMembership(User $user, string $type = '', string $state = '', bool $create = FALSE);

}
