<?php

namespace Drupal\simple_membership;

/**
 * Class SimpleMembershipEvents.
 *
 * Events thrown on particular Simple membership transitions.
 *
 * @package Drupal\simple_membership
 */
final class SimpleMembershipEvents {

  /**
   * Simple membership expires.
   */
  const EXPIRE = 'simple_membership.expire';

  /**
   * Simple membership is created.
   */
  const CREATED = 'simple_membership.create';

  /**
   * Simple membership state changes.
   */
  const STATE_CHANGE = 'simple_membership.state_change';

}
