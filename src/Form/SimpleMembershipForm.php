<?php

namespace Drupal\simple_membership\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for Simple membership edit forms.
 *
 * @ingroup simple_membership
 */
class SimpleMembershipForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\simple_membership\Entity\SimpleMembership $entity */

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\simple_membership\Entity\SimpleMembership $entity */
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Simple membership.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Simple membership.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.simple_membership.canonical', ['simple_membership' => $entity->id()]);
  }

}
