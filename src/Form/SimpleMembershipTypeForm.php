<?php

namespace Drupal\simple_membership\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class SimpleMembershipTypeForm.
 *
 * @package Drupal\simple_membership\Form
 */
class SimpleMembershipTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    /** @var \Drupal\simple_membership\Entity\SimpleMembershipTypeInterface $simple_membership_type */
    $simple_membership_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $simple_membership_type->label(),
      '#description' => $this->t("Label for the Simple membership type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $simple_membership_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\simple_membership\Entity\SimpleMembershipType::load',
      ],
      '#disabled' => !$simple_membership_type->isNew(),
    ];
    $form['workflow'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Workflow'),
      '#description' => $this->t('Enter the Simple membership type workflow definition as a yaml snippet.'),
      '#default_value' => Yaml::dump($simple_membership_type->getSimpleWorkflow(), 4),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $simple_membership_type = $this->entity;
    $simple_membership_type->setSimpleWorkflow(Yaml::parse($form_state->getValue('workflow')));
    $status = $simple_membership_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Simple membership type.', [
          '%label' => $simple_membership_type->label(),
        ]));
        // Clear the cached plugin definitions to ensure new plugin is found.
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Simple membership type.', [
          '%label' => $simple_membership_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($simple_membership_type->toUrl('collection'));
  }

}
