<?php

namespace Drupal\simple_membership\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Simple membership entities.
 *
 * @ingroup simple_membership
 */
class SimpleMembershipDeleteForm extends ContentEntityDeleteForm {

}
