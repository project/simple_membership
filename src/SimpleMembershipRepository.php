<?php

namespace Drupal\simple_membership;

use Drupal\Core\Entity\EntityRepository;
use Drupal\Core\Session\AccountInterface;

/**
 * Repository for Simple memberships.
 */
class SimpleMembershipRepository extends EntityRepository implements SimpleMembershipRepositoryInterface {

  /**
   * {@inheritDoc}
   */
  public function loadMultipleForAccount(AccountInterface $account) {
    return $this->entityTypeManager->getStorage('simple_membership')
      ->loadByProperties(['user_id' => $account->id()]);
  }

}
