<?php

namespace Drupal\simple_membership\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Simple membership type entity.
 *
 * @ConfigEntityType(
 *   id = "simple_membership_type",
 *   label = @Translation("Simple membership type"),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_membership\SimpleMembershipTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_membership\Form\SimpleMembershipTypeForm",
 *       "edit" = "Drupal\simple_membership\Form\SimpleMembershipTypeForm",
 *       "delete" = "Drupal\simple_membership\Form\SimpleMembershipTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\simple_membership\SimpleMembershipTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "simple_membership_type",
 *   bundle_of = "simple_membership",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "workflow",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/simple_membership_type/{simple_membership_type}",
 *     "add-form" = "/admin/structure/simple_membership_type/add",
 *     "edit-form" = "/admin/structure/simple_membership_type/{simple_membership_type}/edit",
 *     "delete-form" = "/admin/structure/simple_membership_type/{simple_membership_type}/delete",
 *     "collection" = "/admin/structure/simple_membership_type"
 *   }
 * )
 */
class SimpleMembershipType extends ConfigEntityBase implements SimpleMembershipTypeInterface {

  /**
   * The Simple membership type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Simple membership type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Simple membership type workflow definition as an array of states and transitions.
   *
   * @var array
   */
  protected $workflow;

  /**
   * {@inheritDoc}
   */
  public function getSimpleWorkflow() {
    return $this->workflow;
  }

  /**
   * {@inheritDoc}
   */
  public function setSimpleWorkflow(array $workflow) {
    $this->workflow = $workflow;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorklowStateLabel(string $workflow_state) {
    $label = $workflow_state;
    if ($this->workflow) {
      if (isset($this->workflow['states'][$workflow_state]['label'])) {
        $label = $this->workflow['states'][$workflow_state]['label'];
      }
    }
    return $label;
  }

  /**
   * {@inheritDoc}
   *
   * @todo Implement configurable options for expired workflow state determination.
   */
  public function isExpired(SimpleMembershipInterface $simple_membership) {
    return $simple_membership->get('workflow_state')->first()->getString() === 'expired';
  }

  /**
   * {@inheritDoc}
   */
  public function shouldCreateNewRevision() {
    return TRUE;
  }

}
