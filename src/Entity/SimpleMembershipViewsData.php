<?php

namespace Drupal\simple_membership\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Simple membership entities.
 */
class SimpleMembershipViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['simple_membership']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Simple membership'),
      'help' => $this->t('The Simple membership ID.'),
    ];

    return $data;
  }

}
