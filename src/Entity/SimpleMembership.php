<?php

namespace Drupal\simple_membership\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entity\Revision\RevisionableContentEntityBase;
use Drupal\simple_membership\EventDispatcherTrait;
use Drupal\simple_membership\Exception\SimpleMembershipFeatureNotImplementedException;
use Drupal\simple_membership\SimpleMembershipEvent;
use Drupal\simple_membership\SimpleMembershipEvents;
use Drupal\user\UserInterface;

/**
 * Defines the Simple membership entity.
 *
 * @ingroup simple_membership
 *
 * @ContentEntityType(
 *   id = "simple_membership",
 *   label = @Translation("Simple membership"),
 *   label_singular = @Translation("Simple membership"),
 *   label_plural = @Translation("Simple memberships"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Simple membership",
 *     plural = "@count Simple memberships",
 *   ),
 *   bundle_label = @Translation("Simple membership type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\simple_membership\SimpleMembershipListBuilder",
 *     "views_data" = "Drupal\simple_membership\Entity\SimpleMembershipViewsData",
 *     "form" = {
 *       "default" = "Drupal\simple_membership\Form\SimpleMembershipForm",
 *       "add" = "Drupal\simple_membership\Form\SimpleMembershipForm",
 *       "edit" = "Drupal\simple_membership\Form\SimpleMembershipForm",
 *       "delete" = "Drupal\simple_membership\Form\SimpleMembershipDeleteForm",
 *     },
 *     "access" = "Drupal\simple_membership\SimpleMembershipAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\simple_membership\SimpleMembershipHtmlRouteProvider",
 *       "revision" = "Drupal\entity\Routing\RevisionRouteProvider",
 *     },
 *   },
 *   base_table = "simple_membership",
 *   revision_table = "simple_membership_revision",
 *   data_table = "simple_membership_field_data",
 *   revision_data_table = "simple_membership_field_revision",
 *   admin_permission = "administer simple_membership entities",
 *   field_ui_base_route = "entity.simple_membership_type.edit_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "revision" = "vid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/simple_membership/{simple_membership}",
 *     "add-form" = "/admin/structure/simple_membership/add/{simple_membership_type}",
 *     "edit-form" = "/admin/structure/simple_membership/{simple_membership}/edit",
 *     "delete-form" = "/admin/structure/simple_membership/{simple_membership}/delete",
 *     "collection" = "/admin/structure/simple_membership",
 *     "revision" = "/admin/structure/simple_membership/{simple_membership}/revisions/{simple_membership_revision}/view",
 *     "version-history" = "/admin/structure/simple_membership/{simple_membership}/revisions",
 *   },
 *   bundle_entity_type = "simple_membership_type"
 * )
 */
class SimpleMembership extends RevisionableContentEntityBase implements SimpleMembershipInterface {

  use EntityChangedTrait;
  use EventDispatcherTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    parent::preCreate($storage, $values);
    if (empty($values['user_id'])) {
      $values['user_id'] = \Drupal::currentUser()->id();

    }
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('The Simple membership type/bundle.'))
      ->setSetting('target_type', 'simple_membership_type')
      ->setRequired(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The owner of the Simple membership entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Data'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['workflow_state'] = BaseFieldDefinition::create('simple_workflow_state')
      ->setLabel(t('Workflow state'))
      ->setDescription(t('The Simple workflow state.'))
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'simple_workflow_state',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'simple_workflow_state',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRevisionable(FALSE);

    $fields['provider'] = BaseFieldDefinition::create('simple_membership_provider_id')
      ->setLabel('Provider plugin/remote ID')
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    if (!$this->isNew() && ($storage->loadUnchanged($this->id())->get('workflow_state')->getValue()) == $this->workflow_state->getValue()) {
      $event = new SimpleMembershipEvent($this);
      $this->getEventDispatcher()->dispatch($event, SimpleMembershipEvents::STATE_CHANGE);
      if ($this->isExpired()) {
        $this->getEventDispatcher()->dispatch($event, SimpleMembershipEvents::EXPIRE);
      }
    }
    parent::preSave($storage);
  }

  /**
   * Return a boolean indicating whether the simple_membership is expired.
   *
   * @return bool
   *   bundle of simple_membership
   */
  public function isExpired() {
    return SimpleMembershipType::load($this->bundle())->isExpired($this);
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    $event = new SimpleMembershipEvent($this);
    $this->getEventDispatcher()->dispatch($event, SimpleMembershipEvents::CREATED);
    parent::postCreate($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function cancel() {
    // @TODO: Implement cancel() method.
    throw new SimpleMembershipFeatureNotImplementedException('Simple membership "Cancel" method not implemented.');

  }

  /**
   * {@inheritdoc}
   */
  public function expireNotice() {
    // @TODO: Implement expireNotice() method.
    throw new SimpleMembershipFeatureNotImplementedException('Simple membership "Expire Notice" method not implemented.');
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    // @TODO: Implement isActive() method.
    throw new SimpleMembershipFeatureNotImplementedException('Simple membership "isActive" method not implemented.');
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorkflow() {
    return SimpleMembershipType::load($this->bundle())->getSimpleWorkflow();
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleWorkflowState() {
    $value = $this->workflow_state->first() ? $this->workflow_state->first()->getValue() : [];
    return reset($value);
  }

  /**
   * {@inheritdoc}
   */
  public function setSimpleWorkflowState(string $workflow_state) {
    $this->workflow_state = $workflow_state;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderPlugin() {
    /** @var \Drupal\simple_membership\Plugin\SimpleMembershipProviderManager $manager */
    $manager = \Drupal::service('plugin.manager.simple_membership_provider.processor');
    return $manager->getInstance($this->get('provider')->first()->getValue());
  }

}
