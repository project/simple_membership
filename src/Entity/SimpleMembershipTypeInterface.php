<?php

namespace Drupal\simple_membership\Entity;

use Drupal\Core\Entity\RevisionableEntityBundleInterface;

/**
 * Provides an interface for defining Simple membership type entities.
 */
interface SimpleMembershipTypeInterface extends RevisionableEntityBundleInterface {

  /**
   * Report whether the Simple membership of this type is considered expired.
   *
   * @param \Drupal\simple_membership\Entity\SimpleMembershipInterface $simple_membership
   *   Report whether the Simple membership of this type is considered expired.
   *
   * @return bool|null
   *   True or false, or NULL if not able to determine.
   */
  public function isExpired(SimpleMembershipInterface $simple_membership);

  /**
   * Return the workflow definition as an array of states and transitions.
   *
   * @return array
   */
  public function getSimpleWorkflow();

  /**
   * Save the workflow definition as an array of states and transitions.
   *
   * @param array $workflow
   *
   * @return $this
   */
  public function setSimpleWorkflow(array $workflow);

  /**
   * Retrieve the label associated with the passed simple workflow state.
   *
   * @param string $workflow_state
   *
   * @return string
   */
  public function getSimpleWorklowStateLabel(string $workflow_state);

}
