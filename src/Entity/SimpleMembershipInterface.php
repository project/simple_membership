<?php

namespace Drupal\simple_membership\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Simple membership entities.
 *
 * @ingroup simple_membership
 */
interface SimpleMembershipInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, RevisionLogInterface {

  /**
   * Gets the Simple membership type.
   *
   * @return string
   *   The Simple membership type.
   */
  public function getType();

  /**
   * Gets the Simple membership creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Simple membership.
   */
  public function getCreatedTime();

  /**
   * Sets the Simple membership creation timestamp.
   *
   * @param int $timestamp
   *   The Simple membership creation timestamp.
   *
   * @return \Drupal\simple_membership\Entity\SimpleMembershipInterface
   *   The called Simple membership entity.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Get a text description of when this Simple membership expires.
   *
   * @return string
   *   A message for the user describing when this Simple membership expires.
   */
  public function expireNotice();

  /**
   * Cancel the current Simple membership immediately.
   *
   * @return \Drupal\simple_membership\Entity\SimpleMembershipInterface
   *   The called Simple membership entity.
   */
  public function cancel();

  /**
   * Return true if this Simple membership is expired.
   *
   * @return bool
   *   Return true if this Simple membership is expired.
   */
  public function isExpired();

  /**
   * Return true if this Simple membership is active.
   *
   * @return bool
   *   Return true if this Simple membership is active.
   */
  public function isActive();

  /**
   * Gets the workflow definition array for the Simple membership type.
   *
   * @return array
   *   The workflow definition array.
   */
  public function getSimpleWorkflow();

  /**
   * Get the current formflow state for this Simple membership.
   *
   * @return string
   *   The current workflow state.
   */
  public function getSimpleWorkflowState();

  /**
   * Set the current workflow state for this Simple membership.
   *
   * @param string $workflow_state
   *   The new workflow state to set.
   *
   * @return $this
   *
   */
  public function setSimpleWorkflowState(string $workflow_state);

  /**
   * Return a configured provider plugin for the Simple membership.
   *
   * @return \Drupal\simple_membership\Plugin\SimpleMembershipProviderInterface|null
   *   Return a configured provider plugin for the Simple membership.
   */
  public function getProviderPlugin();

}
