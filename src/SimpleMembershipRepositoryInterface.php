<?php

namespace Drupal\simple_membership;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Session\AccountInterface;

interface SimpleMembershipRepositoryInterface extends EntityRepositoryInterface {

  /**
   * Load Simple memberships for a user.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account.
   *
   * @return \Drupal\simple_membership\Entity\SimpleMembershipInterface[]
   *   Simple memberships. May or may not be active.
   */
  public function loadMultipleForAccount(AccountInterface $account);

}
