<?php

namespace Drupal\simple_membership\Exception;

/**
 * Class SimpleMembershipFeatureNotImplementedException.
 *
 * @package Drupal\simple_membership\Exception
 */
class SimpleMembershipFeatureNotImplementedException extends \Exception {

  /**
   * SimpleMembershipFeatureNotImplementedException constructor.
   *
   * @param string $feature_name
   *   The feature name.
   */
  public function __construct($feature_name) {
    $message = sprintf(
      'Feature "%s" is not supported by this Simple membership class.',
      $feature_name
    );
    parent::__construct($message);
  }

}
