<?php

namespace Drupal\simple_membership\Exception;

use Drupal\user\Entity\User;

/**
 * Class SimpleMembershipNotFoundException.
 *
 * @package Drupal\simple_membership\Exception
 */
class SimpleMembershipNotFoundException extends \Exception {

  /**
   * SimpleMembershipNotFoundException constructor.
   *
   * @param \Drupal\user\Entity\User|null $user
   *   The user account associated with the Simple membership.
   * @param string $simple_membership_type
   *   The Simple membership type (bundle machine id).
   */
  public function __construct(User $user = NULL, $simple_membership_type = '') {
    if (empty($user)) {
      $message = 'No Simple membership found, no user specified.';
    }
    else {
      $message = sprintf(
        'No %s Simple membership found for "%s".',
        $simple_membership_type,
        $user->label()
      );
    }
    parent::__construct($message);
  }

}
