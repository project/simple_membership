<?php

namespace Drupal\simple_membership;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Simple membership entities.
 *
 * @ingroup simple_membership
 */
class SimpleMembershipListBuilder extends EntityListBuilder {

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SimpleMembershipListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($entity_type, $entity_type_manager->getStorage($entity_type->id()));
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'id' => $this->t('Simple membership id'),
      'name' => $this->t('Member name'),
      'type' => $this->t('Simple membership type'),
      'state' => $this->t('Simple membership state'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\simple_membership\Entity\SimpleMembership $entity */

    $row['id'] = Link::fromTextAndUrl(
      $entity->id(),
      Url::fromRoute(
        'entity.simple_membership.edit_form', [
          'simple_membership' => $entity->id(),
        ]
      )
    );

    $row['name'] = $entity->getOwner()->getAccountName();

    /** @var \Drupal\simple_membership\Entity\SimpleMembershipType $simple_membership_type */
    $simple_membership_type = $this->entityTypeManager->getStorage('simple_membership_type')->load($entity->getType());
    $row['type'] = $simple_membership_type->label();

    $workflow_state = $entity->getSimpleWorkflowState();
    $row['state'] = $simple_membership_type->getSimpleWorklowStateLabel($workflow_state);

    return $row + parent::buildRow($entity);
  }

}
