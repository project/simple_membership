<?php

namespace Drupal\simple_membership;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\user\Entity\User;

/**
 * Class SimpleMembershipManagerService.
 *
 * @package Drupal\simple_membership
 */
class SimpleMembershipManagerService implements SimpleMembershipManagerServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSimpleMembership(User $user, string $type = '', string $state = '', bool $create = FALSE) {

    // Check for existing Simple memberships of this type
    $query = $this->entityTypeManager->getStorage('simple_membership')->getQuery();
    $query->condition('user_id', $user->id());
    if ($type) {
      $query->condition('type', $type);
    }
    if ($state) {
      $query->condition('state', $state);
    }

    if ($existing = $query->accessCheck()->execute()) {
      // Load the Simple memberships.
      $simple_memberships = $this->entityTypeManager->getStorage('simple_membership')->loadMultiple($existing);
      // Return the latest.
      ksort($simple_memberships);
      return array_pop($simple_memberships);
    }

    if ($create && $type) {
      // Create a new Simple membership of specified type for this account.
      /** @var \Drupal\simple_membership\Entity\SimpleMembership $simple_membership */
      $simple_membership = $this->entityTypeManager->getStorage('simple_membership')->create(['type' => $type]);
      $simple_membership->setOwner($user);
      // Save and return the new Simple membership.
      $simple_membership->save();
      return $simple_membership;
    }

    return FALSE;
  }

}
