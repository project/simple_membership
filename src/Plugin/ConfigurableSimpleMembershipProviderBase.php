<?php

namespace Drupal\simple_membership\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Base class for Simple membership provider plugins.
 */
abstract class ConfigurableSimpleMembershipProviderBase extends SimpleMembershipProviderBase implements ConfigurableInterface, DependentPluginInterface, PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => ['simple_membership', 'plugin'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $config = NestedArray::getValue($form_state->getValues(), $form['#parents']);
    if ($config) {
      $this->setConfiguration($config);
    }
  }

}
