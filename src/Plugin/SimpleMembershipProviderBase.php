<?php

namespace Drupal\simple_membership\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Simple membership provider plugins.
 */
abstract class SimpleMembershipProviderBase extends PluginBase implements SimpleMembershipProviderInterface {

}
