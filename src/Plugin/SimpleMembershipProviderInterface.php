<?php

namespace Drupal\simple_membership\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Simple membership provider plugins.
 */
interface SimpleMembershipProviderInterface extends PluginInspectionInterface {

  /**
   * Attempt to configure the plugin with data from the $id.
   *
   * @param string $id
   *   ID recognizable by the plugin.
   *
   * @return self|false
   *   Returns the configured plugin, or false if could not configure.
   */
  public function configureFromId(string $id);

}
