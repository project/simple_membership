<?php

namespace Drupal\simple_membership\Plugin;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\node\NodeInterface;

/**
 * Provides the Simple membership provider plugin manager.
 */
class SimpleMembershipProviderManager extends DefaultPluginManager {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, EntityFieldManagerInterface $entityFieldManager, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct('Plugin/MembershipProvider', $namespaces, $module_handler, 'Drupal\simple_membership\Plugin\SimpleMembershipProviderInterface', 'Drupal\simple_membership\Annotation\SimpleMembershipProvider');

    $this->alterInfo('simple_membership_provider_simple_membership_provider_info');
    $this->setCacheBackend($cache_backend, 'simple_membership_provider_simple_membership_provider_plugins');
    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Find an entity containing a fielded plugin config, by config property.
   *
   * @param string $plugin
   *   The plugin ID.
   * @param string $property
   *   The property name.
   * @param mixed $value
   *   The property value to find.
   *
   * @return array
   *   Array of:
   *   - ContentEntityInterface The entity
   *   - array The config
   *
   * @throws \Exception
   */
  public function findEntityByFieldProperty(string $plugin, string $property, $value) {
    foreach ($this->getFieldInstances($plugin) as $entity_type => $def) {
      foreach ($def as $entityId => $config) {
        if ($config[$property] !== $value) {
          continue;
        }
        $entity = $this->entityTypeManager
          ->getStorage($entity_type)
          ->load($entityId);
        return [$entity, $config];
      }
    }
    throw new \Exception("No matching entity found for config property $property == $value");
  }

  /**
   * The plugin module field type to allow for querying storedconfigurations.
   *
   * @param string $id
   *   The plugin ID to query.
   *
   * @return array
   *   Array of plugin configurations
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFieldInstances(string $id) {
    $instances = $this->entityFieldManager->getFieldMapByFieldType('plugin:simple_membership_provider');
    $tags = [];
    foreach ($instances as $entity_type => $def) {
      foreach ($def as $field_name => $field_config) {
        $query = $this->entityTypeManager->getStorage($entity_type)->getQuery()->condition($field_name . '.plugin_id', $id);
        if ($entity_type == 'node') {
          $query->condition('status', NodeInterface::PUBLISHED);
        }
        if ($result = $query->accessCheck()->execute()) {
          $entities = $this->entityTypeManager->getStorage($entity_type)->loadMultiple($result);
          foreach ($entities as $e) {
            foreach ($e->{$field_name} as $row) {
              if ($row->plugin_id == $id) {
                $tags[$entity_type][$e->id()] = $row->plugin_configuration;
              }
            }
          }
        }
      }
    }
    return $tags;
  }

  /**
   * Retrieve all plugin configurations stored in fields,from entity types.
   *
   * @param string $id
   *   Plugin ID.
   *
   * @return array
   *   Array of plugin configs, if available.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getFieldedEntities(string $id) {
    $configs = [];
    foreach ($this->getFieldInstances($id) as $entity_type => $defs) {
      $configs = array_merge($configs, $defs);
    }
    return $configs;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    // $options should match the structure of the Simple membership's provider field.
    if (empty($options['plugin_id'])) {
      return FALSE;
    }
    try {
      $instance = $this->createInstance($options['plugin_id'])
        ->configureFromId($options['remote_id']);
    }
    catch (\Exception $e) {
      return FALSE;
    }
    return $instance;
  }

}
