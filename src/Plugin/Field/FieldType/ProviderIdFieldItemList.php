<?php

namespace Drupal\simple_membership\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines the 'simple_membership_provider_id' field item list class.
 */
class ProviderIdFieldItemList extends FieldItemList {

}
