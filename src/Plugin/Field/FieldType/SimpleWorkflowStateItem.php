<?php

namespace Drupal\simple_membership\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;

/**
 * Defines the 'simple_workflow' entity field type.
 *
 * @FieldType(
 *   id = "simple_workflow_state",
 *   label = @Translation("Simple workflow state"),
 *   description = @Translation("A field containing a Simple workflow state (plain string value)."),
 *   category = @Translation("Text"),
 *   default_widget = "string_textfield",
 *   default_formatter = "simple_workflow_state"
 * )
 */
class SimpleWorkflowStateItem extends StringItem {

}
