<?php

namespace Drupal\simple_membership\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_membership\Entity\SimpleMembership;
use Drupal\simple_membership_term\Entity\SimpleMembershipTerm;

/**
 * Plugin implementation of the 'options_select' widget for workflow_state field.
 *
 * @FieldWidget(
 *   id = "simple_workflow_state",
 *   label = @Translation("Simple workflow state"),
 *   field_types = {
 *     "simple_workflow_state"
 *   },
 *   multiple_values = FALSE
 * )
 */
class SimpleWorkflowStateWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity): array {
    /** @var \Drupal\simple_membership\Entity\SimpleMembership $entity */

    if (!isset($this->options)) {
      $options = [];
      if ($entity instanceof SimpleMembership || $entity instanceof SimpleMembershipTerm) {
        $entity_type_id = $entity->getType();
        $simple_type_id = $entity->getEntityTypeId() . '_type';
        // $simple_type will be either SimpleMembershipType or SimpleMembershipTermType.
        $simple_type = \Drupal::entityTypeManager()
          ->getStorage($simple_type_id)
          ->load($entity_type_id);
        $workflow = $simple_type->getSimpleWorkflow();
        foreach ($workflow['states'] as $id => $state) {
          $options[$id] = $state['label'] ?? $id;
        }
      }
      $this->options = $options;
    }
    return $this->options;

  }

  /**
   * {@inheritdoc}
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {

    // Massage submitted form values.
    // Drupal\Core\Field\WidgetBase::submit() expects values as
    // an array of values keyed by delta first, then by column, while our
    // widgets return the opposite.

    // Transpose selections from field => delta to delta => field.
    $items[] = [$element['#key_column'] => $element['#value']];

    // @TODO find out why the #parents array has the delta value appended.
    array_pop($element['#parents']);

    $form_state->setValueForElement($element, $items);
  }

}
