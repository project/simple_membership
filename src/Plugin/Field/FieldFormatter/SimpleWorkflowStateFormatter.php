<?php

namespace Drupal\simple_membership\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;
use Drupal\simple_membership\Entity\SimpleMembership;
use Drupal\simple_membership_term\Entity\SimpleMembershipTerm;

/**
 * Plugin implementation of the 'Random_default' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_workflow_state",
 *   label = @Translation("Simple workflow state"),
 *   field_types = {
 *     "simple_workflow_state"
 *   }
 * )
 */
class SimpleWorkflowStateFormatter extends BasicStringFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];

    /** @var \Drupal\Core\Field\FieldItemList $item */
    foreach ($items as $delta => $item) {
      $value = $item->value;
      // Find the simple workflow state label assigned to the specified workflow state.
      $entity = $item->getParent()->getEntity();
      if ($entity instanceof SimpleMembership || $entity instanceof SimpleMembershipTerm) {
        $entity_type_id = $entity->getType();
        $simple_type_id = $entity->getEntityTypeId() . '_type';
        // $simple_type will be either SimpleMembershipType or SimpleMembershipTermType.
        $simple_type = \Drupal::entityTypeManager()->getStorage($simple_type_id)->load($entity_type_id);
        $workflow = $simple_type->getSimpleWorkflow();
        if (isset($workflow['states'][$value]['label'])) {
          $value = $workflow['states'][$value]['label'];
        }
      }

      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|nl2br }}',
        '#context' => ['value' => $value],
      ];
    }

    return $elements;
  }

}
