# Drupal Simple membership module

This module provides a generic Simple membership entity and an optional "Simple membership provider" plugin type.

This module has been forked from the 8.x-1.x branch of the Drupal membership module (https://drupal.org/project.membership).

It has been simplified to remove the dependency on state_machine, and a user interface has been provided to allow for full manual operation.

Like the original, this module provides for a membership entity which can be configured in variety of types. Memberships are assigned to a user account, and can have associated membership terms. Membership terms are intended to provide a time-limited expression of a state of the membership, with a simple expiry and renewal workflow available.

All configuration of the membership and membership_term entities can be enhanced with the addition of custom fields and the simple expiry and renewal workflow can be extended to provide a custom workflow with additional states and transitions.

Installing the simple_membership module will automatically create a basic membership entity type that can be customised or replaced with something new. Installing the simple_membership_term module will create a basic membership term with a simple expiry and renewal workflow. The simple_membership_term can be customised or replaced and the workflow can be extended with additional states and transitions.

There are additional UI modules to provide manual management of memberships and membership terms as required.

### Development

Development occurs on Drupal, utilizing the Drupal.org issue queue.

This project is still very much under development and meta-discussions around future plans and architecture are welcome.
